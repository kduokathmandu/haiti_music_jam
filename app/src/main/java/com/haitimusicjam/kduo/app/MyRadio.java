package com.haitimusicjam.kduo.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.haitimusicjam.kduo.BuildConfig;
import com.haitimusicjam.kduo.R;
import com.haitimusicjam.kduo.helpers.DownloadTracker;
import com.haitimusicjam.kduo.helpers.TransistorKeys;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.onesignal.OneSignal;

import java.io.File;

import co.chatsdk.core.error.ChatSDKException;
import co.chatsdk.core.session.ChatSDK;
import co.chatsdk.core.session.Configuration;
import co.chatsdk.core.session.NetworkManager;
import co.chatsdk.firebase.FirebaseNetworkAdapter;
import co.chatsdk.firebase.file_storage.FirebaseFileStorageModule;
import co.chatsdk.firebase.push.FirebasePushModule;
import co.chatsdk.ui.manager.BaseInterfaceAdapter;
import io.fabric.sdk.android.Fabric;


/**
 * Created by suman on 1/19/17.
 */

public class MyRadio extends Application {


    protected String userAgent;

    private File downloadDirectory;
    private Cache downloadCache;
    private DownloadManager downloadManager;
    private DownloadTracker downloadTracker;

    private static MyRadio instance;
    public InterstitialAd mInterstitialAd;
    private FirebaseAnalytics mFirebaseAnalytics;
    public com.facebook.ads.InterstitialAd facebookInterstitialAd;


    public static MyRadio getInstance() {
        return instance;
    }


    private SharedPreferences prefs;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

//        CrashlyticsCore crashlyticsCore = new CrashlyticsCore.Builder()
//                .disabled(BuildConfig.DEBUG)
//                .build();
//        Fabric.with(this, new Crashlytics.Builder().core(crashlyticsCore).build());

//        final Fabric fabric = new Fabric.Builder(this)
//                .kits(new Crashlytics())
//                .debuggable(false)  // Enables Crashlytics debugger
//                .build();
//        Fabric.with(fabric);
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        // Initialize the SDK before executing any other operations,
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);


        prefs = getSharedPreferences("com.suman.radio.app.tms", Context.MODE_PRIVATE);


//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        loadInterstitialAds();
        userAgent = Util.getUserAgent(this, "ExoPlayerDemo");

        Context context = getApplicationContext();

// Create a new configuration
        Configuration.Builder builder = new Configuration.Builder(context);

// Perform any configuration steps (optional)
        builder.firebaseRootPath("prod");
        builder.publicRoomCreationEnabled(true);
        builder.defaultName("Haiti User Default");
        builder.defaultUserAvatarUrl("https://ibb.co/WsfgdrD");
        builder.anonymousLoginEnabled(true);

// Initialize the Chat SDK
        try {
            ChatSDK.initialize(builder.build(), new FirebaseNetworkAdapter(), new BaseInterfaceAdapter(context));
        }
        catch (ChatSDKException e) {
        }

// File storage is needed for profile image upload and image messages
        FirebaseFileStorageModule.activate();
        FirebasePushModule.activate();

    }



    public void loadInterstitialAds() {


        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.mInterstitialAd));
        AdRequest request;


        boolean isPersonalized = prefs.getBoolean(TransistorKeys.PREF_ADS_TYOE, true);


        if (isPersonalized) {
            request = new AdRequest.Builder()
//                    .addTestDevice("8EB0C61DCD1AE87EFE2B450885E7B09A")
                    .build();

        } else {
            request = new AdRequest.Builder()
//                    .addTestDevice("8EB0C61DCD1AE87EFE2B450885E7B09A")
                    .addNetworkExtrasBundle(AdMobAdapter.class, getNonPersonalizedAdsBundle())
                    .build();
        }

        mInterstitialAd.loadAd(new AdRequest.Builder()
                .build());

    }


    public Bundle getNonPersonalizedAdsBundle() {
        Bundle extras = new Bundle();
        extras.putString("npa", "1");
        return extras;
    }

}
