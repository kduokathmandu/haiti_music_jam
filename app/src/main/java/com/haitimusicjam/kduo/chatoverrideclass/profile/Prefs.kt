package com.haitimusicjam.kduo.chatoverrideclass.profile

import android.content.Context

class Prefs{


    companion object {

        val PREF_FILE = "KDUO_CHAT"

        @JvmStatic
        fun setString(context: Context, key: String, value: String) {
            val settings = context.getSharedPreferences(PREF_FILE, 0)
            val editor = settings.edit()
            editor.putString(key, value)
            editor.apply()
        }
    }
}