package com.haitimusicjam.kduo.chatoverrideclass.chat

import android.content.Intent
import android.os.Bundle
import android.util.Log
import co.chatsdk.core.session.ChatSDK
import co.chatsdk.core.types.AccountDetails
import co.chatsdk.ui.main.MainActivity
import com.haitimusicjam.kduo.chatoverrideclass.login.LoginExtras
import com.haitimusicjam.kduo.util.Constant
import kotlinx.android.synthetic.main.chat_sdk_activity_view_pager.*

class ChatActivity: MainActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("CheckingVall", "herer")
        super.onCreate(savedInstanceState)
        fabRadio.setOnClickListener {
            val intent = Intent(this, com.haitimusicjam.kduo.MainActivity::class.java)
            intent.putExtra(Constant.FROMCHAT,true)
            intent.putExtra(Constant.EXTRAS,LoginExtras.extras)
            intent.putExtra(Constant.LOGINSTATUS,true)
            startActivity(intent)
            finish()
        }
    }

}