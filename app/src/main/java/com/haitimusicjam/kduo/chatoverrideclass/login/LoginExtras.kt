package com.haitimusicjam.kduo.chatoverrideclass.login

import java.util.HashMap

class LoginExtras{

    companion object {
        @JvmStatic
        var extras: HashMap<String, Any>? = null
        @JvmStatic
        var loginStatus: Boolean = false
    }

    init {
        this
    }
}