package com.haitimusicjam.kduo.chatoverrideclass.login

import android.content.Intent
import android.util.Log
import co.chatsdk.ui.login.LoginActivity
import com.haitimusicjam.kduo.chatoverrideclass.chat.ChatActivity
import com.haitimusicjam.kduo.util.Constant
import java.util.HashMap


class KduoLogin: LoginActivity(){


    override fun openActivity(extras: HashMap<String, Any>?) {
        LoginExtras.extras = extras!!
        LoginExtras.loginStatus = true
        val intent = Intent(this, ChatActivity::class.java)
        intent.putExtra(Constant.EXTRAS, extras)

        startActivity(intent)
        finish()
    }

}